Using Frizby for Integration tests.

Does my API work as expected?

Sometimes you need the DB to be set up and tear down before and after the Integration test/s.

This project sets up MySQL for Frizby and shows setup and teardown examples.